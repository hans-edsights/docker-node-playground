# Use an official Alpine Linux as a base image
FROM alpine:latest

# Install Git
RUN apk add npm --no-cache

# Set the working directory in the container
WORKDIR /workspace

CMD ["bash"]
