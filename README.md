# Docker Node Playground

1. Build the Docker image 
- First let's build the docker image and tag it with `alpine-node`

2. Run the script
- Now that we have the image saved let's run `run.sh`
- This should put you in the prompt of your running container


3. Let's create some temp files
- Now let's run the `create-files.js` to create some temporary files

4. Let's count those files 
- Now let's count how many files are in the directory by running the `count-files.js` to see how many files were created

5. Now write me a program that finds the second largest and second smallest word in this word file `words.txt`

