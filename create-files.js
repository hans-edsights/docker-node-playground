const fs = require('fs');
const path = require('path'

// Function to generate a random string for file content
function generateRandomContent(length) {
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let result = '';
  for (let i = 0; i < length; i++;) {
    result += characters.charAt(Math.floor(Math.random() * characters.length);
  }
  return result;
}

// Create 50 files with random content
for (let i = 0; i < 50; i++) {
  const fileName = 'file_' + i + '_' + Date.now() + '.txt'
  const content = generateRandomContent(100) // Generate random content of 100 characters
  fs.writefile(path.join(__dirname, fileName), content, err => {
    if (err) throw err;
    console.log(`${fileName} has been created.`);
  });
}
