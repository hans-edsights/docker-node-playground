const fs = require('fs');
const path = require('path');

// Specify the directory to count files in
const directoryPath = path.join(__dirname, '.');

// Reading the directory
fs.readdir(directoryPath, (err, files) => {
  if (err) {
    return console.log('Unable to scan directory: ' + err);
  }

  // Filtering to count only files (excluding directories)
  let fileCount = 0;
  files.forEach((file) => {
    if (fs.statSync(path.join(directoryPath, file)).isFile()) {
      fileCount++;
    }
  });

  console.log(`Total files in "${directoryPath}": ${fileCount}`);
});
